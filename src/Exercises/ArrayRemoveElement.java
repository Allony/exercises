package Exercises;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayRemoveElement {
    public static void main(String[] args) {
        int[] arr = {2, 21, 1, 2, 3, 44, 2, 3, 4, 1, 2};
        arr = removeElement(arr, 2);
        System.out.println(Arrays.toString(arr));
    }

    public static int[] removeElement(int[] list, int element) {
        int count = 0;
        for (int i = 0; i < list.length; i++) {
            if (list[i] == element) {
                count++;
            } else {
                list[i - count] = list[i];
            }
        }
        int[] newList = new int[list.length - count];
        System.arraycopy(list, 0, newList, 0, newList.length);
       return newList;
    }
}
