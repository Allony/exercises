package Exercises;

import java.util.Comparator;

public class BinaryTree<T, E> {

    private Entry<T, E> nullEntry;
    private Comparator<E> comparator;
    private int size = 0;

    public BinaryTree(Comparator<E> comparator) {
        nullEntry = new Entry<>();
        this.comparator = comparator;
    }

    private static class Entry<E, K> {
        E element;
        K key;

        Entry<E, K> left;
        Entry<E, K> right;
        Entry<E, K> parent;

       public Entry() {
        }

       public Entry(E element, Entry<E, K> parent, K key) {
            this.element = element;
            this.parent = parent;
            this.key = key;

        }
    }

    public int size() {
        return size;
    }

    public void add(T obj, E key) {
        Entry<T, E> buffer = nullEntry.right;

        while (buffer != null) {
            int index = comparator.compare(key, buffer.key);

            switch (index) {
                case 1:
                    if (buffer.right != null) {
                        buffer = buffer.right;
                        break;
                    } else {
                        buffer.right = new Entry<>(obj, buffer, key);
                        size++;
                        return;
                    }

                case -1:
                    if (buffer.left != null) {
                        buffer = buffer.left;
                        break;
                    } else {
                        buffer.left = new Entry<>(obj, buffer, key);
                        size++;
                        return;
                    }
                case 0:
                    return;
            }
        }

        nullEntry.right = new Entry<>(obj, nullEntry, key);
        size++;
    }

    private Entry<T, E> getEntry(E key) {
        Entry<T, E> buffer = nullEntry.right;

        if (buffer == null)
            return null;

        while (buffer != null) {
            int index = comparator.compare(key, buffer.key);

            switch (index) {
                case 1:
                    buffer = buffer.right;
                    break;
                case -1:
                    buffer = buffer.left;
                    break;
                case 0:
                    return buffer;
            }
        }

        return null;
    }

    public T get(E key) {
        Entry<T, E> buffer = getEntry(key);

        if (buffer == null)
            return null;

        return buffer.element;
    }

    public void remove(E key) {
        Entry<T, E> buffer = getEntry(key);

        if (buffer == null)
            return;

        Entry<T, E> min;
        int event = comparator.compare(buffer.key, buffer.parent.key);

        if (buffer.right != null) {
            min = getKeyInBrunch(buffer.right, "Left");
            min.parent.left = null;
            min.parent = buffer.parent;
            buffer.right.parent = min;

            if (buffer.left != null)
                buffer.left.parent = min;
        } else if (buffer.left != null) {
            min = getKeyInBrunch(buffer.left, "Right");
            min.parent.right = null;
            min.parent = buffer.parent;
            buffer.left.parent = min;
        } else {
            min = null;
        }

        if (event > 0) {
            buffer.parent.right = min;
        } else {
            buffer.parent.left = min;
        }
        size--;
    }

    private Entry<T, E> getKeyInBrunch(Entry<T, E> parent, String direction) {
        Entry<T, E> buffer = parent;

        switch (direction) {
            case "Left":
                while (buffer.left != null)
                    buffer = buffer.left;
                break;
            case "Right":
                while (buffer.right != null)
                    buffer = buffer.right;
                break;
        }

        return buffer;
    }
}
