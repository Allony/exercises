package Exercises;

import java.util.ArrayList;
import java.util.Arrays;

public class SimpleNums {
    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(2);
        arr.add(3);
        arr.add(5);
        arr.add(7);

        for (int i = 7; i <= 1000; i++) {
            boolean isSimple = true;

            for (Integer integer : arr) {
                if (i % integer == 0) {
                    isSimple = false;
                    break;
                }
            }

            if (isSimple) {
                arr.add(i);
            }
        }

        System.out.println(Arrays.toString(arr.toArray()));
    }
}
