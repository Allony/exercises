package Exercises;

public class WeightNumbers {
    public WeightNumbers(int[] nums, int[] weight) {
        int k = 0;

        for (int value : weight) {
            k += value;
        }

        double random = Math.random() * k;
        double sum = 0;

        for (int i = 0; i < nums.length; i++) {
            if (random > sum) {
                sum += weight[i];
            } else {
                System.out.println(nums[i]);
                break;
            }
        }
    }
}
