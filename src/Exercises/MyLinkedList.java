package Exercises;


public class MyLinkedList<T> {

    private Entry<T> nullEntry;
    private int size;

    public MyLinkedList() {
        nullEntry = new Entry<>();
        size = 0;
    }

    private static class Entry<E> {
        E element;
        Entry<E> next;
        Entry<E> prev;

        Entry() {
            this.element = null;
            this.prev = this;
            this.next = this;
        }

        Entry(E element, Entry<E> next, Entry<E> prev)
        {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }
    }

    private int size() {
        int size = 0;
        Entry<T> getSize = nullEntry;
        while (getSize.next != nullEntry) {
            getSize = getSize.next;
            size++;
        }
        return size;
    }

    public int getSize() {
        return size;
    }

    public Object get(int i) {
        Entry<T> entry = getEntry(i);

        if (entry == null)
            return "Out of bounds";

        return entry.element;
    }

    private Entry<T> getEntry(int i) {
        if (i > size || i < 0)
           return null;

        Entry<T> searchEntry;

        if (i >= size / 2) {
            searchEntry = nullEntry.prev;

            for (int j = size - 1; j != i; j--) {
                searchEntry = searchEntry.prev;
            }
        } else {
            searchEntry = nullEntry.next;
            for (int j = 0; j != i; j++) {
                searchEntry = searchEntry.next;
            }
        }
        return searchEntry;
    }

    public boolean add(T o) {
        Entry<T> prevEntry = nullEntry.prev;
        Entry<T> newEntry = new Entry<>(o, nullEntry, prevEntry);
        nullEntry.prev = newEntry;
        prevEntry.next = newEntry;
        size = size();
        return true;
    }

    public void remove(int i) {
        Entry<T> removable = getEntry(i);

        if (removable == null)
            return;

        removable.prev.next = removable.next;
        removable.next.prev = removable.prev;
        size = size();
    }
}
