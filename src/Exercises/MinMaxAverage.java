package Exercises;

public class MinMaxAverage {
    public static void main(String[] args) {
        int[] arr = {5, 12, 54, 32, 5, -23, 43, 1};
        int max = arr[0];
        int min = arr[0];
        int average;
        int aver = 0;

        for (int value : arr) {
            if (max < value)
                max = value;

            if (min > value)
                min = value;
        }

        average = (max + min) / 2;
        System.out.println(average);
        int averageMin = arr[0];

        for (int value : arr) {
            int buffMin = Math.abs(value - average);

            if (buffMin < averageMin) {
                aver = value;
                averageMin = buffMin;
            }
        }

        System.out.println("Max - " + max);
        System.out.println("Min - " + min);
        System.out.println("Average - " + aver);
    }
}
