package Exercises;

public class Vector {
    private double x;
    private double y;
    private double z;

    Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double getLength() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public static double scalarMultiply(Vector o1, Vector o2) {
        return o1.getX() * o2.getX() + o1.getY() * o2.getY() + o1.getZ() * o2.getZ();
    }

    public static double vectorAngle(Vector o1, Vector o2) {
        double numerator = scalarMultiply(o1, o2);
        double denominator = o1.getLength() * o2.getLength();
        return numerator / denominator;
    }

    public static Vector getSum(Vector o1, Vector o2) {
        return new Vector(o1.getX() + o2.getX(), o1.getY() + o2.getY(), o1.getZ() + o2.getZ());
    }

    public static Vector getDiff(Vector o1, Vector o2) {
        return new Vector(o1.getX() - o2.getX(), o1.getY() - o2.getY(), o1.getZ() - o2.getZ());
    }

    public static Vector[] getVectorArray(int length) {
        Vector[] array = new Vector[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = new Vector(Math.random(), Math.random(), Math.random());
        }
        return array;
    }
}
