package Algoritms;

import java.util.Scanner;

public class BinaryAlgorithm {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] list = new int[100];

        for (int i = 0; i < list.length; i++) {
            list[i] = i;
        }
        System.out.println("Загадайте число от 0 до 100");
        System.out.println(binarySearch(list, scanner.nextInt()));
    }

    private static int binarySearch(int[] list, int key) {
        int low = 0;
        int high = list.length - 1;
        int mid = 0;
        int result;
        while (low != high) {
            mid = (low + high) / 2;
            result = list[mid];
            if (result == key) {
                return mid;
            } else if (result < key) {
                low = mid + 1;
            } else if (result > key) {
                high = mid - 1;
            } else {
                return 0;
            }
        }
        return mid;
    }
}
