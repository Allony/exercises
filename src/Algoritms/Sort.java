package Algoritms;

import java.util.Arrays;
import java.util.Comparator;


public class Sort {
    public static void sort(int[] list) {
        int buffer;

        for (int i = 0; i < list.length; i++) {
            int biggest = list[0];
            int index = 0;

            for (int x = 1; x < list.length - i; x++) {
                if (list[x] > biggest) {
                    biggest = list[x];
                    index = x;
                }
            }
            buffer = list[list.length - (i + 1)];
            list[list.length - (i + 1)] = biggest;
            list[index] = buffer;
        }
    }

    public static void sortRecursion(int[] list, int iter) {
        if (iter == list.length) {
            return;
        }

        int buffer;
        int biggest = list[0];
        int index = 0;

        for (int x = 0; x < list.length - iter; x++) {
            if (list[x] > biggest) {
                biggest = list[x];
                index = x;
            }
        }

        buffer = list[list.length - (iter + 1)];
        list[list.length - (iter + 1)] = biggest;
        list[index] = buffer;
        System.out.println(Arrays.toString(list));
        sortRecursion(list, iter + 1);
    }

    public static void fastSort(Object[] list) {
        /*
        int length = list.length;
        if (length <= 1) {
            return;
        } else if (length == 2) {
            int buffer;
            if (list[0] > list[1]) {
                buffer = list[0];
                list[0] = list[1];
                list[1] = buffer;
            } else {
                int point = list[(length / 2) - 1];
                ArrayList<Integer> lower = new ArrayList<>();
                ArrayList<Integer> higher = new ArrayList<>();
                ArrayList<Integer> equals = new ArrayList<>();
                for (int i = 0; i < length; i++) {
                    if (list[i] < point) {
                        lower.add(list[i]);
                    } else if (list[i] > point) {
                        higher.add(list[i]);
                    } else {
                        equals.add(list[i]);
                    }
                    fastSort(lower.toArray());
                }
            }
        }

         */
    }

    public static void comparatorSort(int[] list, Comparator comparator, int length) {
        for (int i = 0; i < list.length - 1; i++) {
            int res = comparator.compare(list[i], list[i + 1]);

            if (res == 1) {
                int buffer = list[i];
                list[i] = list[i + 1];
                list[i + 1] = buffer;
                System.out.println(Arrays.toString(list));
            }
        }

        if (length > 0) {
            comparatorSort(list, comparator, length - 1);
        }
    }
}
